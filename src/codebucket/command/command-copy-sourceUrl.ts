import { CommandBase } from './command-base';
import * as vscode from 'vscode';

export class CopyBitbucketSourceUrlCommand extends CommandBase {
    protected async execute(): Promise<void> {
        const backend = await this.getBackend();
        const remote = await backend.findBitbucketSite();
        const rev = await backend.findCurrentRevision();
        const filePath = this.getFilePath(backend.root);
        const fileUrl = remote.getSourceUrl(rev, filePath, this.getLineRanges());
        await vscode.env.clipboard.writeText(fileUrl);
    }
}
